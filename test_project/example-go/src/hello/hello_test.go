// go test
// go test -v
// go test -v -run=[run func name]
// go test -v -bench="."

package hello

import (
	"fmt"
	"strconv"
	"testing"
)

// Go test file
func Test_A(t *testing.T) {
	name, price := NewFruit("FF", 600)
	if price != nil {
		t.Fatal("got errors:", price)

	}

	if name == nil {
		t.Error("car should be nil")
	}
}

func Test_B(t *testing.T) {
	// Log test
	if 1 == 2 {
		t.Log("Some text to show")
	} else {
		t.Log("~~")
	}
}

func printInt2String01(num int) string {
	return fmt.Sprintf("%d", num)
}

func printInt2String02(num int64) string {
	return strconv.FormatInt(num, 10)
}
func printInt2String03(num int) string {
	return strconv.Itoa(num)
}

// func Test_Fail(t *testing.T) {
// 	// Result fail
// 	t.Fail()

// 	t.Error("Error text here")
// }

// b.N default num: 100000000
// Benchmark test must be this format
func Benchmark_test(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NewFruit("1", 10)
	}
}

func BenchmarkPrintInt2String01(b *testing.B) {
	for i := 0; i < b.N; i++ {
		printInt2String01(100)
	}
}

func BenchmarkPrintInt2String02(b *testing.B) {
	for i := 0; i < b.N; i++ {
		printInt2String02(int64(100))
	}
}

func BenchmarkPrintInt2String03(b *testing.B) {
	for i := 0; i < b.N; i++ {
		printInt2String03(100)
	}
}
