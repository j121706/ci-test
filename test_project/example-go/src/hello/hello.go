package hello

import (
	"errors"
)

// Fruit struct
type Fruit struct {
	Name  string
	Price float32
}

// New Object
func NewFruit(name string, price float32) (*Fruit, error) {
	if name == "" {
		return nil, errors.New("missing name")
	}

	return &Fruit{
		Name:  name,
		Price: price,
	}, nil
}

// SetName set Fruit name
func (c *Fruit) SetName(name string) string {
	if name != "" {
		c.Name = name
	}

	return c.Name
}

func Print() {
	println("HELLO!!")
}
