# @file CMakeLists.txt
# @author LCY
# @date 2020.07
# @brief 
# Cmake setting file for C

cmake_minimum_required(VERSION 3.8)

set(This Demo_C_Test)

# Set benckmark on or off
# true for benchmark test mode
# false for c unit test mode
message("BENCHMARK_TEST_SWITCH=${BENCHMARK_TEST_SWITCH}")

if(BENCHMARK_TEST_SWITCH)
    # benchmark test mode
    file(GLOB Sources
        test/benchmark.cpp
    )
else()
    # c++ unit test mode
    file(GLOB Sources
        test/test.cpp
    )
endif()

add_executable(${This} ${Sources})

target_link_libraries(${This} PUBLIC
    gtest_main
    # benchmark
    GT
)

add_test(
    NAME ${This}
    COMMAND ${This}
)