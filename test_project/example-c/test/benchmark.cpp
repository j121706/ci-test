/**
 * @file benchmark.cpp
 * @author your name (you@domain.com)
 * @date 2020-08-11
 * @brief
 * https://github.com/google/benchmark
 *
 */
#include <benchmark/benchmark.h>

extern "C"{
  static void BM_StringCreation_C(benchmark::State& state) {
    for (auto _ : state)
      std::string empty_string;
  }
}
// Register the function as a benchmark
BENCHMARK(BM_StringCreation_C);

// Define another benchmark
extern "C"{
  static void BM_StringCopy_C(benchmark::State& state) {
    std::string x = "hello_C";
    for (auto _ : state)
      std::string copy(x);
  }
}
BENCHMARK(BM_StringCopy_C);

BENCHMARK_MAIN();