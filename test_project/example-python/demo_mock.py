import unittest
from unittest import mock
from hello import func_test_A, func_test_B

not_a_related_value = 9999

class TestMockFuncs(unittest.TestCase):
    @mock.patch('hello.func_test_A')
    def test_patch(self, mock_func_test_A):
        mock_func_test_A.return_value = 10
        self.assertEqual(func_test_B(not_a_related_value), 100)

if __name__ == "__main__":
    unittest.main()