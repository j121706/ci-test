# @file makefile
# @author LCY
# @date 2020.07
# @brief 

## ----- C & C++ Commands ----- 

# google test clone variables
GTSOURCE = https://github.com/google/googletest.git
GTPATH	 = googletest

# google benchmark clone variables
GBSOURCE = https://github.com/google/benchmark.git
GBPATH	 = google-benchmark

# cmake variables
CMAKE_PATH = build
CTEST_PATH = test_project/example-c++

TEST_NAME  = DemoTest


gtest-install: 
	git clone $(GTSOURCE) $(GTPATH)

gb-install: 
	git clone $(GBSOURCE) $(GBPATH)

g-uninstall:
	rm -rf google-benchmark/
	rm -rf googletest/

call-result:
	@${CMAKE_PATH}/${CTEST_PATH}/Debug/${TEST_NAME}.exe

clean-build:
	rm -rf build/
	mkdir build

## ----- Golang Commands ----- 

GOPATH = test_project/example-go

# go-build:
# 	@go build $(GOPATH)/*.go

go-clean:
	@rm $(GOPATH)/*.exe