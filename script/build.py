# cmake docker build location:
# /usr/local/cmake/build

import os


def cmake_build():
    cmake_build_loc = "/usr/local/cmake/build "

    cmd = "cmake --build "
    cmd += cmake_build_loc
    cmd += "--config Debug"
    os.system(cmd)

if __name__ == '__main__':
    cmake_build()
